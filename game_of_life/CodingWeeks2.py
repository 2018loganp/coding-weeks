import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.animation as ani
import argparse as ag

def affichage(universe): #Affiche l'univers sur 1 generation
    X=[i for i in range(len(universe))]
    Y=[j for j in range(len(universe[0]))]
    fig, ax=plt.subplots()
    for i in X:
        for j in Y:
            if universe[i][j]==1:
                rect=patch.Rectangle((i,j),1,1)
                ax.add_patch(rect)
    plt.show()


from pytest import *
import numpy as np
import random2 as rd

def generate_universe(size): #Creation d'un univers vide
    universe=np.zeros(size)
    return(universe)

def test():
    assert np.array_equal((generate_universe((4,4))),[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])


def ajoute_graine_universe(size,amorce): #Insere aleatoirement l'amorce dans l'univers
    universe = generate_universe(size)
    x=rd.randint(0,size[0]-len(amorce))
    y=rd.randint(0,size[1]-len(amorce[0]))
    for i in range(len(amorce)):
        for j in range(len(amorce[0])):
            #print(x+i)
            #print(y+j)
            universe[x+i][y+j]=amorce[i][j]
    return universe

def ajoute_graine_precis(size, amorce, x, y): #Insere de maniere precis l'amorce dans l'univers
    universe= generate_universe(size)
    for i in range(len(amorce)):
        for j in range(len(amorce[0])):
            #print(x+i)
            #print(y+j)
            universe[x-i][y+j]=amorce[i][j]
    return universe


pentimino=[[0,1,1],[1,1,0],[0,1,0]]


#print(affichage(ajoute_graine_universe((10,10),pentimino)))

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
    "block": [[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]
    ],
    "blinker": [[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]
    ],
    "glider": [[0,1,0],[0,0,1],[1,1,1,]]
}

#print(seeds.get("glider"))


def survival(x,y,universe): #Definit si une cellule vit ou meurt
    new_valeur=universe[x][y]
    if x==0:
        if y==0:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants= -1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante
                for i in [0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        elif y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [0,1]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif y==0:
        if x==len(universe)-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif x==len(universe)-1:
        if y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    else:
        if universe[x][y]==0:
            Voisins_vivants=0
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    Voisins_vivants+=universe[x+i][y+j]
            if Voisins_vivants==3:
                new_valeur=1
        else:
            Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    Voisins_vivants+=universe[x+i][y+j]
            if Voisins_vivants==3 or Voisins_vivants==2:
                new_valeur=1
            else:
                new_valeur=0
    return new_valeur

'''On traite tous les cas particuliers, puis le cas general'''

def universe_adapter(dicto, clef,):   #Cree un univers assez grand pour une amorce dans la dictionnaire d'amorce
    size=(dicto[clef][0]+2, dicto[clef][1]+2)
    ajoute_graine_universe(size,dicto[clef])



def generation(universe): #Avance tout l'univers d'une generation
    universe2=np.zeros((len(universe),len(universe[0])))
    for x in range(len(universe)):
        for y in range(len(universe[0])):
            universe2[x][y]=survival(x,y,universe)
    return universe2

print(generation([[0,0,0],[1,1,1],[0,0,0]]))

#print(survival(0,0,[[0,0,0],[1,1,1],[0,0,0]]))

def game_life_simulate(iterations, universe): #Avance tout l'univers d'un nombre choisi de generations
    for i in range(iterations):
        universe=generation(universe)
    return universe


def visualisation(universe,i): #Animation de l'univers
    fig, ax = plt.subplots()
    ax.set_xlim(0,len(universe[0]))
    ax.set_ylim(0,len(universe))
    X=[x for x in range(len(universe[0]))]
    Y=[y for y in range(len(universe))]
    for h in range(i):
        print(universe)
        for x in X:
            for y in Y:
                if universe[x][y]==1:
                    rect=patch.Rectangle((x,y),1,1)
                    ax.add_patch(rect)
        plt.pause(0.05)
        for x in X:
            for y in Y:
                if universe[x][y]==1:
                    rect=patch.Rectangle((x,y),1,1,color='white') #Pour remettre la chose à zéro
                    ax.add_patch(rect)

        universe=generation(universe)
    plt.show()




def visualisation2(size,seed_position,seed_type,colour='blue',iterations=30,time_intervalle=0.3,save=False):

    #Une tentative echouée d'enregistrement de Gif

    FFMpegWriter = ani.writers['ffmeg']
    metadata=dict(title='Game of Life', artist='Peter et Léa', comment='On est trop fort')
    writer=FFMpegWriter(fps=20, metadata=metadata)
    fig, ax = plt.subplots()
    universe=ajoute_graine_precis(size,seed_type,seed_position[0],seed_position[1])
    ax.set_xlim(0,len(universe[0]))
    ax.set_ylim(0,len(universe))

    with writer.saving(fig, "Victoire.mp4",i):
        X=[x for x in range(len(universe))]
        Y=[y for y in range(len(universe[0]))]
        for h in range(iterations):
            for x in X:
                for y in Y:
                    if universe[x][y]==1:
                        rect=patch.Rectangle((x,y),1,1,colour)
                        ax.add_patch(rect)
            writer.grab_frame()
            plt.pause(time_intervalle)
            #ax.plot(np.zeros((len(universe),len(universe[0]))))
            for x in X:
                for y in Y:
                    if universe[x][y]==1:
                        rect=patch.Rectangle((x,y),1,1,color='white') #Pour remettre la chose à zéro
                        ax.add_patch(rect)

            universe=generation(universe)
    plt.show()



#Cette fonction tente d'utiliser FuncAnimation, mais ne fonctionne pas
def gif(universe, j=100,colour='b',time_intervalle=100):
    U=universe    #Test pour avoir un gif facilement
    C=colour
    fig, ax = plt.subplots()
    ax.set_xlim(0,len(universe[0]))
    ax.set_ylim(0,len(universe))
    line, = ax.plot([],[],lw=2)
    k=j
    t=time_intervalle
    def init():
        line.set_data([],[])
        return (line,)
    def animate(i):
        X=[x for x in range(len(U))]
        Y=[y for y in range(len(U[0]))]
        for x in X:
            for y in Y:
                if U[x][y]==1:
                    rect=patch.Rectangle((x,y),1,1,C)
                    ax.add_patch(rect)

    anim=ani.FuncAnimation(fig,animate,init_func=init,interval=t,blit=True,frames=k,repeat=False)
    plt.show()

#gif(seeds["blinker"],100)



'''parser = ag.ArgumentParser()
parser.add_argument("size", help="Les dimension du jeu", type=tuple)
parser.add_argument("seed_position", help='Ou placez vous lamorce ?',type=tuple)
parser.add_argument("seed_type", help='Le type de amorce choisi', type=list)
parser.add_argument("--colour",help='Coleur des cellules vivantes')
parser.add_argument("--interations",help='Nombre de generations',type=int)
parser.add_argument("--time_intervalle",help="Temps entre generations",type=int)
parser.add_argument("--save",help='Vous voulez savuer un petit gif fort sympa ?',type=bool)
args=parser.parse_args()'''

#La fonction principale, ou l'utilisateur est demandé progressivement les données
'''def main():
    y=int(input("Please enter the number of lines"))
    x=int(input("Please entre the number of columns"))
    size=(y,x)
    seed=input("Please entre your choice of seed")
    wherex=int(input("On what column do you want to start your seed?"))
    wherey=int(input("On what line do you want to start your seed?"))
    where=(wherey,wherex)
    iteration=float(input("How many generations?"))
    time=float(input("How much time between generations?"))
    C=input("What colour would you like?")
    save=bool(input("Do you want to save? (True to save)"))
    visualisation2(size,where,seed,C,iteration,time,save)'''



#Une deuxieme tentative avec FuncAnimation pour faire fonctionner les gifs, un deuxieme echec
def ausecours(universe):
    fig = plt.figure()
    U=universe
    fig.set_dpi(100)
    fig.set_size_inches(7, 6.5)
    #patch = plt.Circle((5, -5), 0.75, fc='y') Rectangles ! ! ! ! ! !! ! !! ! !  !
    ax = plt.axes(xlim=(0, len(universe)), ylim=(0, len(universe[0])))

    def init():
        rect=patch.Rectangle((0,0),len(universe),len(universe[0]),color='white')
        ax.add_patch(rect)
        return patch,

    def animate(i):
        V=game_life_simulate(i,U)
        X=[x for x in range(len(V))]
        Y=[y for y in range(len(V[0]))]
        for x in X:
            for y in Y:
                if V[x][y]==1:
                    rect=patch.Rectangle((x,y),1,1)
                    ax.add_patch(rect)
        return patch,

    anim = ani.FuncAnimation(fig, animate, init_func=init,frames=360, interval=20, blit=True)

    plt.show()

#ausecours(seeds["blinker"])


#Le programme mis sur Slack, mis qui ne marche pas
def animate():
    fig = plt.figure()

    swap = 0
    tab = [[0]]
    im = plt.imshow(tab, animated=True)

    def updatefig(*args):
        nonlocal tab
        if(swap == 0): swap = 1
        else:
            if (tab[0][0] == 0): tab[0][0] = 1
            else: tab[0][0] = 0
        im.set_array(tab)
        return im,

    anim = ani.FuncAnimation(fig, updatefig, interval=50, blit=True)
    plt.show()

#animate()



#visualisation(seeds["infinite"],60)


#La fin du Main
#if __name__=="__main__":
    #main()




def test3(size,seed_name,x_start,y_start,cmap_choice='Greys',speed=200,n=100,dico=seeds):
    universe=ajoute_graine_precis(size,seed_name,x_start,y_start)#Génération de l'unvivers
    print(universe)
    fig=plt.figure()
    im=plt.imshow(universe,cmap=cmap_choice,interpolation="none", animated=True) #Affiche l'univers au départ
    def update_image(universe): #Fonction de mise à jour de l'animation
        universe=generation(universe)
        im.set_array(universe)
        return im,

    anim=ani.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
    plt.axis('off')
    plt.show()

#test3((10,10),seeds['blinker'],5,5)


visualisation(ajoute_graine_precis((10,10),seeds['blinker'],5,5),30)
