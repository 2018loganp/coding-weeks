


import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.animation as ani
import argparse as ag

def affichage(universe): #Affiche l'univers sur 1 generation
    X=[i for i in range(len(universe))]
    Y=[j for j in range(len(universe[0]))]
    fig, ax=plt.subplots()
    for i in X:
        for j in Y:
            if universe[i][j]==1:
                rect=patch.Rectangle((i,j),1,1)
                ax.add_patch(rect)
    plt.show()
import numpy as np
import random2 as rd

def generate_universe(size): #Creation d'un univers vide
    universe=np.zeros(size)
    return(universe)

def test():
    assert np.array_equal((generate_universe((4,4))),[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])


def ajoute_graine_universe(size,amorce): #Insere aleatoirement l'amorce dans l'univers
    universe = generate_universe(size)
    x=rd.randint(0,size[0]-len(amorce))
    y=rd.randint(0,size[1]-len(amorce[0]))
    for i in range(len(amorce)):
        for j in range(len(amorce[0])):
            #print(x+i)
            #print(y+j)
            universe[x+i][y+j]=amorce[i][j]
    return universe

def ajoute_graine_precis(size, amorce, x, y): #Insere de maniere precis l'amorce dans l'univers
    universe= generate_universe(size)
    for i in range(len(amorce)):
        for j in range(len(amorce[0])):
            #print(x+i)
            #print(y+j)
            universe[x-i][y+j]=amorce[i][j]
    return universe


#pentimino=[[0,1,1],[1,1,0],[0,1,0]]


#print(affichage(ajoute_graine_universe((10,10),pentimino)))

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
    "block": [[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]
    ],
    "blinker": [[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]
    ],
    "glider": [[0,1,0],[0,0,1],[1,1,1,]]
}

#print(seeds.get("glider"))


def survival(x,y,universe): #Definit si une cellule vit ou meurt
    new_valeur=universe[x][y]
    if x==0:
        if y==0:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants= -1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante
                for i in [0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        elif y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [0,1]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [0,1]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif y==0:
        if x==len(universe)-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0,1]:
                    for j in [0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif x==len(universe)-1:
        if y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
        else:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0]:
                    for j in [-1,0,1]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    elif y==len(universe[0])-1:
            if universe[x][y]==0:
                Voisins_vivants=0
                for i in [-1,0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3:
                    new_valeur=1
            if universe[x][y]==1:
                Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
                for i in [-1,0,1]:
                    for j in [-1,0]:
                        Voisins_vivants+=universe[x+i][y+j]
                if Voisins_vivants==3 or Voisins_vivants==2:
                    new_valeur=1
                else:
                    new_valeur=0
    else:
        if universe[x][y]==0:
            Voisins_vivants=0
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    Voisins_vivants+=universe[x+i][y+j]
            if Voisins_vivants==3:
                new_valeur=1
        else:
            Voisins_vivants=-1 #Pour ne pas compter la cellule elle meme comme cellule voisine vivante"
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    Voisins_vivants+=universe[x+i][y+j]
            if Voisins_vivants==3 or Voisins_vivants==2:
                new_valeur=1
            else:
                new_valeur=0
    return new_valeur

'''On traite tous les cas particuliers, puis le cas general'''

def universe_adapter(dicto, clef,):   #Cree un univers assez grand pour une amorce dans la dictionnaire d'amorce
    size=(dicto[clef][0]+2, dicto[clef][1]+2)
    ajoute_graine_universe(size,dicto[clef])



def generation(universe): #Avance tout l'univers d'une generation
    universe2=np.zeros((len(universe),len(universe[0])))
    for x in range(len(universe)):
        for y in range(len(universe[0])):
            universe2[x][y]=survival(x,y,universe)
    return universe2

#print(generation([[0,0,0],[1,1,1],[0,0,0]]))

#print(survival(0,0,[[0,0,0],[1,1,1],[0,0,0]]))

def game_life_simulate(iterations, universe): #Avance tout l'univers d'un nombre choisi de generations
    for i in range(iterations):
        universe=generation(universe)
    return universe




def test3(size,seed_name,x_start,y_start,cmap_choice='Greys',speed=200,n=100,dico=seeds):
    universe=ajoute_graine_precis(size,seed_name,x_start,y_start)#Génération de l'unvivers
    print(universe)
    fig=plt.figure()
    im=plt.imshow(universe,cmap=cmap_choice,interpolation="none", animated=True) #Affiche l'univers au départ
    def update_image(universe): #Fonction de mise à jour de l'animation
        universe=generation(universe)
        im.set_array(universe)
        return im,

    anim=ani.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
    plt.axis('off')
    plt.show()

test3((10,10),seeds["blinker"],5,5)

