from Tkinter import *
import random2 as rd
import numpy as np



def initialise(size,amorce): #on initialise un univers etant donne une taille et une amorce placee aleatoirement dans l'univers
    universe = np.zeros(size)
    x=rd.randint(0,size[0]-len(amorce))
    y=rd.randint(0,size[1]-len(amorce[0]))
    for i in range(len(amorce)):
        for j in range(len(amorce[0])):
            universe[x+i][y+j]=amorce[i][j]
    return universe


#nb_lignes=int(input('combien de lignes voulez-vous pour votre jeu de la vie ?'))

#gameoflife=Tk()
#gameoflife=Toplevel(gameoflife)
#gameoflife.grid()
#univers_jeu=Canvas(gameoflife,width=1000)
#gameoflife.geometry("%dx%d%+d%+d" % (1000,1000,0,0)) #l'univers a toujours la meme taille, quel que soit le nombre de cellules
#taille_cellule=1000//nb_lignes #on adapte la taille des cellules en fonction de leur nombre
#rectangle=univers_jeu.create_rectangle(0,0,taille_cellule,taille_cellule,fill='black')
#univers_jeu.pack()


def representation(universe):
    gameoflife=Tk()
    gameoflife=Toplevel(gameoflife)
    gameoflife.grid()
    univers_jeu=Canvas(gameoflife,width=1000)
    gameoflife.geometry("%dx%d%+d%+d" % (1000,1000,0,0)) #l'univers a toujours la meme taille, quel que soit le nombre de cellules
    taille_cellule=1000//len(universe) #on adapte la taille des cellules en fonction de leur nombre
    #rectangle=univers_jeu.create_rectangle(0,0,taille_cellule,taille_cellule,fill='black')
    for i in range(len(universe)):
        for j in range(len(universe[0])):
            if universe[i][j]==1:
                rectangle=univers_jeu.create_rectangle(j*taille_cellule,i*taille_cellule,(j+1)*taille_cellule,(i+1)*taille_cellule,fill='black')
                univers_jeu.pack
    gameoflife.mainloop()



#gameoflife.mainloop()

#representation([[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]])
